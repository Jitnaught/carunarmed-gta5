﻿using System;
using GTA;
using GTA.Native;

namespace CarUnarmed
{
	public class CarUnarmed : Script
	{
		bool wasInVehicle = false;

		public CarUnarmed()
		{
			Interval = 50;
			Tick += CarUnarmed_Tick;
		}

		void CarUnarmed_Tick(object sender, EventArgs e)
		{
			if (Game.IsLoading || Game.IsScreenFadedOut) return;

			Ped plrPed = Game.Player.Character;

			if (plrPed == null || !plrPed.Exists() || plrPed.IsDead)
			{
				wasInVehicle = false;
				return;
			}

			bool inVehNow = plrPed.IsInVehicle();

			if (!wasInVehicle && inVehNow)
			{
				plrPed.Weapons.Select(WeaponHash.Unarmed, true);
			}

			wasInVehicle = inVehNow;
		}
	}
}
